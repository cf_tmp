<?php
/**
 * WordPress template for the Circular Facts project.
 *
 * The template arranges the information of the website into thematic
 * `circuits'. The diagram on the front page is built from this structure.
 * Most of this file points in that direction.
 *
 * @package Circular_Facts
 */

/** Tell WordPress to run cf_setup() when the 'after_setup_theme' hook is run.
 * This is copy paste from the twentyten template and stackoverflow
 * http://wordpress.stackexchange.com/questions/9/how-do-i-add-support-to-my-theme-for-custom-menus
 */
add_action("after_setup_theme", "cf_setup");

/** This theme uses wp_nav_menu() in several locations:
 * index.php
 * page.php
 * programme.php
 * single.php
 *
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * @uses add_theme_support() To add support for post thumbnails feed links.
 * @uses register_nav_menus() To add support for navigation menus.
 */
function cf_setup() {
	add_theme_support("automatic-feed-links");
	register_nav_menus(array("primary" => "Primary Navigation"));
}

/**
 * Returns a random codepoint from the `work' selection.
 *
 * It's used by the programme and single views to print a codepoint after the
 * title.
 */
function the_codepoint() {
	$work_codepoints = array(0x25a2, 0x25a4, 0x25a5, 0x25a6, 0x25a9, 0x25ef, 0x25c9, 0x25b3, 0x25bc, 0x25d0, 0x25d3, 0x25b1);
	$mycodepoint = array_rand($work_codepoints, 2);
	printf("<span class=\"codepoint\">&#x%s;</span>", dechex($work_codepoints[$mycodepoint[0]]));
}

/**
 * This is the basic building block of the front page diagram.
 *
 * This function prints a `work', connected by a dashed line to a (possibly
 * empty) list of corresponding `media'.
 *
 * These `works' are put together by work2unicode() into `circuits'.
 *
 * In turn, the front page diagram is composed of several `circuits' and is
 * put together by diagram()
 *
 * Random codepoints are chosen for the `work' and for each of the `media', if
 * any.
 *
 * @param array $media a work's media as produced by the read_data function
 */
function media2unicode($media) {
	$work_codepoints = array(0x25a2, 0x25a4, 0x25a5, 0x25a6, 0x25a9, 0x25ef, 0x25c9, 0x25b3, 0x25bc, 0x25d0, 0x25d3, 0x25b1);
	$media_codepoints = array(0x259e, 0x2595, 0x2583, 0x258f, 0x259e, 0x258c, 0x259e, 0x259e, 0x2595, 0x258f, 0x258e, 0x259a, 0x258a, 0x258e, 0x258b);
	$dashed_line = 0x2508;
	$mycodepoint = array_rand($work_codepoints, 2);
	printf("<a href=\"%s\" class=\"diagram\">%s", "?p=" . $media[0],
		"&#x" . dechex($work_codepoints[$mycodepoint[0]]) . ";");
	if (count($media) != 1) {
		printf(" %s ", "&#x" . dechex($dashed_line) . ";");
	}
	for ($i = 1; $i < count($media); $i++) {
		$medium_codepoint = array_rand($media_codepoints, 2);
		print "&#x" . dechex($media_codepoints[$medium_codepoint[0]]) . ";";
	}
	print "</a>";
	print " ";
}

/**
 * This is one of two buildings block of the front page diagram.
 *
 * It is called by the diagram function in conjuction with
 * circuit2auxiliaries.
 *
 * Puts together a `circuit' by concatenating a number of `works'.
 *
 * Prints an arrow between `works'.
 *
 * Randomizes the `works'.
 *
 * @param array $works an artist's works as produced by the read_data function
 */
function work2unicode($works) {
	$work_codepoints = array(0x25a2, 0x25a4, 0x25a5, 0x25a6, 0x25a9, 0x25ef, 0x25c9, 0x25b3, 0x25bc, 0x25d0, 0x25d3, 0x25b1);
	$right_arrow = 0x25b8;
	for ($i = 0; $i < count($works); $i++) {
		$mycodepoint = array_rand($work_codepoints, 2);
		printf("<a href=\"%s\" class=\"diagram\">%s", "?p=" . $works[$i][0],
			"&#x" . dechex($work_codepoints[$mycodepoint[0]]) . ";");
		print "</a>";
		print " ";
		if ($i != count($works) - 1) {
			printf("%s ", "&#x" . dechex($right_arrow) . ";");
		}
	}
}

/**
 * This is one of two buildings block of the front page diagram.
 *
 * It is called by the diagram function in conjuction with work2unicode.
 *
 * Prints the attachments and the external links that have been assigned to
 * one `circuit'.
 *
 * There are two loops. One handles the bookmarks. The other handles the
 * `auxiliaries', ie the attachments of the posts.
 *
 * @param array $works an artist's works as produced by the read_data function
 */
function circuit2auxiliaries($artist) {
	# any $artist[myindex][0] is ok, as they're all in the same category
	$my_categories = get_the_category($artist[0][0]);
	$my_current_category = $my_categories[0]->name;
	$media_codepoints = array(0x259e, 0x2595, 0x2583, 0x258f, 0x259e, 0x258c, 0x259e, 0x259e, 0x2595, 0x258f, 0x258e, 0x259a, 0x258a, 0x258e, 0x258b);
	$my_urls = get_bookmarks("category_name=$my_current_category");
	shuffle($my_urls);
	$my_media = get_attachments_by_media_tags("media_tags=$my_current_category");
	if (count($my_urls) != 0 || $my_media) {
		printf(" %s ", "&#x" . dechex(0x2508) . ";");
	}
	# loop 1
	foreach ($my_urls as $my_url) {
		$mycodepoint = array_rand($media_codepoints, 2);
		printf("<a href=\"%s\" class=\"diagram\" target=\"_blank\">%s</a>", $my_url->link_url,
"&#x" . dechex($media_codepoints[$mycodepoint[0]]) . ";");
	}
	# loop 2
	if ($my_media) {
		shuffle($my_media);
		foreach ($my_media as $medium) {
			$mycodepoint = array_rand($media_codepoints, 2);
			if ($medium->post_mime_type == "application/pdf") {
				printf("<a href=\"%s\" class=\"diagram\">%s</a>", $medium->guid, "&#x" . dechex($media_codepoints[$mycodepoint[0]]) . ";");
			} else {
				printf("<a href=\"%s\" class=\"diagram\" rel=\"lightbox\">%s</a>", $medium->guid, "&#x" . dechex($media_codepoints[$mycodepoint[0]]) . ";");
			}
		}
	}
	if (count($my_urls) != 0 || $my_media) {
		print " ";
	}
}

/**
 * Puts together the diagram by concatenating a number of `circuits'.
 *
 * Prints opening/closing brackets at the beginning/end of each circuit.
 * Prints an x between circuits. It's the first call in a small chain
 * reaction:
 *
 * <code>
 * diagram -> work2unicode
 * diagram -> circuit2auxiliaries
 * </code>
 *
 * These two functions are called one after the other. They're both sent the
 * complete array of an artist's works as returned by the read_data function.
 *
 * @param array $artists categories as returned by the read_data function
 */
function diagram($artists) {
	shuffle($artists);
	for ($i = 0; $i < count($artists); $i++) {
		$left_bracket = 0x255f;
		$right_bracket = 0x2562;
		$x = 0x2573;
		if ($i != 0) {
			printf("%s ", "&#x" . dechex($right_bracket) . ";");
			printf("%s ", "&#x" . dechex($x) . ";");
		}
		printf("%s ", "&#x" . dechex($left_bracket) . ";");
		work2unicode($artists[$i]);
		circuit2auxiliaries($artists[$i]);
	}
	print "&#x" . dechex($right_bracket) . ";";
}

/**
 * Place website data in an array suitable for the Unicode diagram.
 *
 * It takes in an array as returned by the `get_categories' query. Every
 * artist is assigned one category. The return value of the function is an
 * array of arrays that looks like this:
 *
 * <code>
 * artist
 * 		work
 *			work->ID
 * </code>
 *
 * The bottommost array is superflous. It's a leftover from a previous
 * version. There's no need for an array of id's for a given work.
 *
 * We used to print the corresponding attachments after every work. Now we
 * print a `total sum' of attachments at the end of every circuit.
 *
 * I've no time at the moment to look further into this :(
 *
 * @param array $categories the website's categories
 */
function read_data($categories) {
	$artist_index = 0;
	foreach ($categories as $artist_id) {
		$works = get_posts("category=$artist_id->cat_ID");
		$works_index = 0;
		foreach ($works as $work_post) {
			$work_id = $work_post->ID;
			$artists[$artist_index][$works_index][] = $work_id;
			$works_index++;
		}
		$artist_index++;
	}
	return $artists;
}
