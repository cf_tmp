<?php define('WP_USE_THEMES', false); get_header(); ?>

<?php
$categories = get_categories();
#shuffle($categories);
$artists = read_data($categories);
?>

<div id="container">
<div id="all">
<div id="title_front">
<?php bloginfo('name'); ?>
</div><!--title-->
<div id="diagram">
<?php
diagram($artists);
?>
</div><!--diagram-->
<div id="colophon">
Circular Facts is a collaborative endeavor between three European contemporary
art organizations: Casco – Office for Art, Design and Theory (Utrecht),
Objectif Exhibitions (Antwerp), The Showroom (London) in partnership with Kunst
Halle Sankt Gallen and Electric Palm Tree.
</div><!--colophon-->
</div><!--all-->

<div id="aside">
<div id="menu">
<ul>
	<li>
		Index
<?php
$args = array("theme_location" => "primary", "container" => "");
wp_nav_menu($args);
?>
	</li>
</ul>
</div>

</div><!--aside-->
</div><!--container-->


<?php get_footer(); ?>
