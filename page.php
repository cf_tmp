<?php get_header(); ?>
<!--
<div id="debug">
&nbsp;
</div>
-->

<div id="container">
<div id="all">

<div id="title">
<?php bloginfo('name'); ?>
</div>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div id="work_container">
<div id="work">

&nbsp;

</div><!--work-->
</div><!--work_container-->
<div id="narrative">

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<?php the_content(); ?>

				<div class="entry-meta">

<p>
<?php edit_post_link( __( 'Edit', 'sandbox' ), "\n\t\t\t\t\t<span class=\"edit-link\">", "</span>" ) ?>
</p>

				</div><!--entry-meta-->
				</div><!--post-php the_ID(); ?>" php post_class(); ?>-->

</div><!--narrative-->

<?php endwhile; ?>

</div><!--all-->

<div id="aside">
<div id="menu">
<ul>
	<li>
		<a href="<?php bloginfo('url') ?>">Index</a>
<?php
$args = array("theme_location" => "primary", "container" => "");
wp_nav_menu($args);
?>
	</li>
</ul>
</div>

</div><!--aside-->
</div><!--container-->

<?php get_footer(); ?>
