<?php get_header(); ?>
<!--
<div id="debug">
&nbsp;
</div>
-->

<div id="container">
<div id="all">

<div id="title">
<?php bloginfo('name'); ?>
</div>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div id="work_container">
<div id="work">
<?php the_title();
print " ";
the_codepoint();?>
</div><!--work-->
</div><!--work_container-->
<div id="narrative">

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<?php the_content(); ?>

				<div class="entry-meta">

<p>
<?php edit_post_link( __( 'Edit', 'sandbox' ), "\n\t\t\t\t\t<span class=\"edit-link\">", "</span>" ) ?>
</p>

				</div><!--entry-meta-->
				</div><!--post-php the_ID(); ?>" php post_class(); ?>-->

</div><!--narrative-->

<?php
foreach((get_the_category()) as $category) {
	$my_category = $category->name;
}
?>



<?php endwhile; ?>
</div><!--all-->

<div id="aside">
<div id="menu">
<ul>
	<li>
		<a href="<?php bloginfo('url') ?>">Index</a>
<?php
$args = array("theme_location" => "primary", "container" => "");
wp_nav_menu($args);
?>

	</li>
</ul>
</div><!--menu-->
<div id="placeholder_menu_container">
<div id="placeholder_menu">
&nbsp;
</div><!--placeholder_menu_container-->
</div><!--placeholder_menu-->
<div id="menu2">
<ul>
<?php
$args = array("media_tags" => $my_category, "orderby" => "name");
$my_auxiliaries = get_attachments_by_media_tags($args);
if ($my_auxiliaries || wp_list_bookmarks("echo=&title_li=&categorize=0&category_name=$my_category")) {
print "\t<li>\n";
	print "Auxiliary";
	print "<ul>\n";
}
if ($my_auxiliaries) {
	$media_codepoints = array(0x259e, 0x2595, 0x2583, 0x258f, 0x259e, 0x258c, 0x259e, 0x259e, 0x2595, 0x258f, 0x258e, 0x259a, 0x258a, 0x258e, 0x258b);
	foreach ($my_auxiliaries as $media) {
		$mycodepoint = array_rand($media_codepoints, 2);
		if ($media->post_mime_type == "application/pdf") {
			printf("<li><a href=\"%s\" title=\"%s\">%s</a> %s</li>\n",
				$media->guid, $media->post_excerpt, $media->post_title,
				"&#x" . dechex($media_codepoints[$mycodepoint[0]]) . ";");
		} else {
			printf("<li><a href=\"%s\" title=\"%s\" rel=\"lightbox\">%s</a> %s</li>\n",
				$media->guid, $media->post_excerpt, $media->post_title,
				"&#x" . dechex($media_codepoints[$mycodepoint[0]]) . ";");
		}
	}
}

$my_urls = get_bookmarks("category_name=$my_category");
foreach ($my_urls as $my_url) {
	$work_codepoints = array(0x25a2, 0x25a4, 0x25a5, 0x25a6, 0x25a9, 0x25ef, 0x25c9, 0x25b3, 0x25bc, 0x25d0, 0x25d3, 0x25b1);
	$mycodepoint = array_rand($work_codepoints, 2);
	printf("<li><a href=\"%s\" title=\"%s\" target=\"_blank\">%s</a> %s</li>\n",
		$my_url->link_url, $my_url->link_description, $my_url->link_name, "&#x" . dechex($work_codepoints[$mycodepoint[0]]) . ";");
}
?>

</ul>

	</li>
</ul>
</div><!--menu2-->

</div><!--aside-->
</div><!--container-->

<?php get_footer() ?>
