<?php
/*
Template Name: Programme
*/
define('WP_USE_THEMES', false); get_header();
?>

<div id="container">
<div id="all">
<div id="title_front">
<?php bloginfo('name'); ?>
</div><!--title_front-->
<?php
$works = get_posts("numberposts=0");
foreach ($works as $work) { ?>
		<div class="work_front"><a href="<?php print $work->guid; ?>">
		<?php print $work->post_title; ?></a> <?php the_codepoint(); ?>
		</div><!--work_front-->
<?php
};
?>
<div id="colophon">
Circular Facts is a collaborative endeavor between three European contemporary
art organizations: Casco – Office for Art, Design and Theory (Utrecht),
Objectif Exhibitions (Antwerp), The Showroom (London) in partnership with Kunst
Halle Sankt Gallen and Electric Palm Tree.
</div><!--colophon-->
</div><!--all-->

<div id="aside">
<div id="menu">
<ul>
	<li>
		<a href="<?php bloginfo('url') ?>">Index</a>
<?php
$args = array("theme_location" => "primary", "container" => "");
wp_nav_menu($args);
?>
	</li>
</ul>
</div><!--menu-->

</div><!--aside-->
</div><!--container-->

<?php get_footer(); ?>
